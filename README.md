# README #

SPT(SDN Prototyper Tool) é um Framework baseado nas ferrramentas Mininet e OVS(OpenVSwitch), sua principal função é facilitar a prototipagem e teste de topologia de rede SDN. O framework possui duas interfaces gráficas, uma para facilitar a prototipagem, a outra ajuda na instalação e inicialização de serviços do ambiente de teste. 
Sua arquitetura é dividida em duas camadas: camada de execução e camada de criação.

Sua arquitetura é baseada em cliente e servidor. Onde o servidor é executado no S.O Linux, Distribuição Ubuntu, que origina a camada de execução e o cliente acessa as funcionalidades da ferramenta por meio do navegador Web 
que tem a função da camada de criação. A comunicação entre a camada de criação e a camada de execução é remotamente.

A camada de execução foi configurada e executada no Ubuntu 12.04.5 LTS Gnome virtualizado, enquanto que as requisições foram realizadas na maquina hospedeira, utilizando web browser. 

Sumário:

    1. Pré-requisitos
    2. Orientações Necessárias Antes da Instalação
    3. Configurar VM em modo Bridge
    4. Configurar interface de Rede
    4. Instalação
    5. Iniciar a ferramenta SPT
    6. Acessar Funcionalidade da Ferramenta SPT
    7. Manual de uso

## 1. Pré-requisito ##

    1.1 - Apartir do Ubuntu 12.04.5 LTS
    1.2 - python 2.7 
    1.3 - git
    1.4 - Ferramenta SPT.
  
## 2. Orientações Necessárias Antes da Instalação ##

  É indicado que a maquina física tenha mais de 2 Gb de memoria RAM para suportar a virtualização.

  É recomendado que a instalação do framework aconteça em um Sistema Operacional Linux Ubuntu 12.04.5 LTS virtualizado no Virtual Box; e configurar a VM para trabalhar em modo bridge. 
  A motivação maior para se trabalhar com a plataforma Linux é a sua integração com as ferramenta mininet e OVS(OpenVswitch). Essas duas ferramentas são essencias para protoripar a rede modelada na interface de interação da ferramenta SPT.
  
  Estamos disponibilizando o arquivo .OVA para download, [Link Download](https://drive.google.com/file/d/0B2yoWRkzCtqBa2ZtcHhpMkIza28/view?usp=sharing),  do ubuntu server 12.04 LTS com interface GTK, 
  pronto para ser importado pelo VirtualBox, a fim de agilizar o processo de instalação e uso da ferramenta SPT. 
  
  Login e password da imagem virtual do ubuntu server 12.04 LTS, obtida no paragrafo anterior
  
	login:esp
	password:*.*esp
  
  Durante a instação a interface gráfica se faz necessário, por isso o ubunto server estar com interface gráfica GTK.
  
  Os passos seguinte considera que o usuário esteja no ubuntu virtualizado.
  
  

  Depois de logado, inicialize a interface gráfica com o seguinte comando:
  
	sudo startxfce4
  
  Faça o Download da ferramenta SPT, é recomendado que o Download seja realizado no diretório do usuário, localizada do diretório \home:

    2.1 - Execute o terminal, Crt + Alt + t em Sistemas Operacionais Ubuntu, e digite os seguintes comandos:
    2.2 - cd $HOME  
    2.3 - sudo apt-get update
    2.4 - sudo apt-get install git
    2.4 - git clone https://bitbucket.org/marlon_pimentel/spt-sdn-prototyper-tool
    
  Caso a atualização informe o seguinte erro:
  
    Falhou ao buscar bzip2:/var/lib/apt/lists/partial
    
  Execute o seguinte comando:
  
    sudo rm -rf /var/lib/apt/lists/*
    
  Em seguida atualize novamente:
  
    sudo apt-get update
  
  Configure sua VM para trabalhar em modo Bridge. A seção seguinte, 3. Configurar VM em modo Bridge, mostra como configurar.
    

## 3. Configurar VM em modo Bridge ##

  A configuração é mostrada na imagem config_VM.png, para acessar a imagem siga os seguintes passos: 
  
  Primeiramente acesse o diretório spt-sdn-prototyper-tool, obtido na seção "2. Recomendações" e "subseção 2.3", em seguida o diretório config\_vm.
  Siga o seguinte procedimento para realizar essa tarefa:
  
    3.1 - Execute o terminal, Crt + Alt + t em Sistemas Operacionais Ubuntu, e digite o seguinte comando:
    3.2 - nautilus
    3.3 - Ao abrir o gerênciador de arquivo, clique no diretório spt-sdn-prototyper-tool e em seguida clique no diretório config_vm.   
  
  Caso esteja utilizando um gerenciador de arquivos diferente do nautilus, a recomendação é utilizar a interface gráfica do seu Desktop para abrir o gerenciador de arquivo para acessar 
  o diretório spt-sdn-prototyper-tool e config_vm. 
    
## 4. Configurar interface de Rede ##

  Caso esteja em uma rede que o IP é atribuído via DHCP. Você não precisa fazer nem uma configuração. O próprio serviço DHCP vai atribui um IP para sua maquina virtual que foi configurada em modo bridge.

  Caso esteja em uma rede que é preciso configurar o IP manualmente. É necessário que o arquivo /etc/network/interface da maquina virtual, seja configurado com o mesmo padrão da sua maquina física que se encontra 
  configurada com o mesmo padrão da rede. Com o arquivo interface em modo de edição modifique apenas o campo destinado ao numero IP.
  
  A seguir é mostrado uma maneira de editar o arquivo /etc/network/interface via terminal:
  
    sudo vim /etc/network/interface
  
  [Comandos Utilizado no vim](https://www.vivaolinux.com.br/dica/Editor-Vim-Introducao-e-trabalhando-com-Vim)
  
  
  Para verificar o IP atribuido à sua maquina, execute o seguinte comando no terminal:
  
    ifconfig

## 5. Instalação ##

  Execute o terminal, Crt + Alt + t em Sistemas Operacionais Ubuntu, acesse o diretório spt-sdn-prototyper-tool, obtido na seção "2. Recomendações", em seguida acesse o diretório camada_execucao. 
  A baixo é mostrado os comandos para realizar a operação e a instalação:

    5.1 - cd $HOME

    5.1 - cd spt-sdn-prototyper-tool/camada_execucao

    5.2 - Executar como usuario sudo o arquivo install_esp.py(Instalar Dependências): sudo ./install_spt.py.

    5.3 - No decorrer da instalação uma interface será mostrada e os passos presentes precisam ser executados para finalizar a instalação.

    5.4 - Ao finalizar os processos do item 5.3 click no botão sair e feche a execução do servidor de instação, com a seguinte combinação de tecla Ctrl + C.

## 6. Iniciar Ferramenta SPT ##

  Apos a intalação ser finalizada feche todas as interfaces abertas e em seguida acesse o diretorio onde se encontra o diretorio camada_execucao, execute o script start_spt.py como usuario sudo.

    6.1 sudo python start_spt.py

## 7. Acessar Funcionalidade da Ferramenta SPT remotamente ##
  
  Abrir um navegador e digitar o IP do servidor e a porta 4080.
    
    Ex: 192.168.1.5:4080.
    
  Para verificar o IP da maquina, execute o terminal e insira o comando:
  
    ifconfig
    
## 8. Manual de Uso ##

  Ao acessar a camada de criação por meio do navegador Web, as funcionalidades são apresentadas por secção. A secção default que é apresentada, se chama "Modelar Topologia", 
  essa secção permite ao usuário modelar sua topologia conforme sua necessidade. A seguir são descritos algumas orientações para modelar a topologia: 
    
    8.1 - Para criar nodos e arestas o usuário precisa clicar no ícone "editar", que se encontra logo abaixo da secção "Modelar Topologia". 
    
    8.2 - Para adicionar nodo, clique em "Adicionar Nó" e em seguida clique no retângulo a baixo, a camada de criação vai apresentar uma caixa de dialago que permite nomear o nodo, 
    selecionar o tipo(Switch, host ou controlador) e salvar.
    
    8.3 - Para conectar um Nodo ao outro, clique em "Adicionar aresta" em seguida clique no primeiro nodo de preferência e arraste até o segundo nodo. 
    
    8.4 - Já as funções "remover selecionado" e "edita Nó" são acionadas ao clicar em um determinado Nó. 
    
    8.5 - Acontece algo parecido com a orientação 7.4, quando o usuário clica na aresta, a diferença é que no lugar de "editar Nó" 
    aparece "Editar aresta". As descrições apresentadas permitem ao usuário modelar topologia. 

    As outras secções são "Adicionar Regra OpenFlow", "Monitorar Switch" e secção "Topologia" que é apresentada quando o usuário clicar no botão finalizar modelagem.