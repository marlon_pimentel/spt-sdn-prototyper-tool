#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import sys
import time
from flask import Flask
import json
import simplejson as json
import re
import subprocess
from subprocess import Popen, PIPE
import os, fnmatch
import commands
import argparse
from collections import defaultdict
#from mininet.net import Mininet
#from mininet.node import Controller
#from mininet.cli import CLI
#from mininet.log import setLogLevel, info

####### dependencia graph topologia ########
import os,sys
sys.path.append('..')
sys.path.append('/usr/lib/graphviz/python/')
sys.path.append('/usr/lib64/graphviz/python/')
import gv
from pygraph.classes.graph import graph
from pygraph.classes.digraph import digraph
from pygraph.algorithms.searching import breadth_first_search
from pygraph.readwrite.dot import write
##########################################

PROJECT_DIR, PROJECT_MODULE_NAME = os.path.split(
    os.path.dirname(os.path.realpath(__file__))
)

FLASK_JSONRPC_PROJECT_DIR = os.path.join(PROJECT_DIR, os.pardir)
if os.path.exists(FLASK_JSONRPC_PROJECT_DIR) \
        and not FLASK_JSONRPC_PROJECT_DIR in sys.path:
    sys.path.append(FLASK_JSONRPC_PROJECT_DIR)

from flask_jsonrpc import JSONRPC

app = Flask(__name__)
jsonrpc = JSONRPC(app, '/api', enable_web_browsable_api=True)

class variable:
    display = 'xterm -T "CLI - Mininet" -geometry 100 -e'
    sumn = 'sudo mn'
    mnc = 'sudo mn -c'
    su = 'sudo'
    dir_ovs = '$HOME/ovs'
    topo = 'topo_t.py'
    dir_esp = '$HOME/esp'
    output = open(os.devnull,'w')
    def __init__(self,addvar):
        self.addvar = addvar

def t_json(topo):
    j = json.dumps(topo, sort_keys=True, indent = 4)
    js = json.loads(j)
    print "envia para APPtopo", js
    return js

def graph_topo(tp):
    
    subprocess.call("rm graph_topo.png && rm static/graph/graph_topo.png",shell=True,stdout=variable.output,stderr=variable.output)
    print "Criar topologia em Grafo"
    print tp
    t =  tp
    ht = []
    gr = graph()

    for i in t:
       ht.append(i[0])
       ht.append(i[1])

    rrp = list(set(ht))

    gr.add_nodes(rrp)   

    for i in t:
       gr.add_edge((i[0],i[1]))
    
    print "Debug --- 87" 
    dot = write(gr)
    gvv = gv.readstring(dot)
    gv.layout(gvv,'dot')
    print "\n91\n"
    gv.render(gvv,'png','graph_topo.png')
    print "\n93\n"
    #st, order = breadth_first_search(gr, root="s1")
    print "\n95\n"
    #gst = digraph()
    print "\n96\n"
    #gst.add_spanning_tree(st)
    print "\n97\n"
    #dot = write(gst)
    #gvv = gv.readstring(dot)
    print "\nDebug --- 98\n"
    #gv.layout(gvv,'dot')
    #gv.render(gvv,'png','topo_BFS.png')
    subprocess.call("sudo chmod +777 graph_topo.png",shell=True,stdout=variable.output,stderr=variable.output)
    subprocess.call("mv graph_topo.png static/graph",shell=True,stdout=variable.output,stderr=variable.output)
    print "ttttttttttttttt",t
    return t

def Start_Topo(stp): #executar arquivo que cria a topologia
    varstp = stp

    if varstp['start'] == "topology":
       print "ESP ------ Prototipar Topologia"
       print variable.display
       print variable.topo
       # call(["xterm -T 'ESP' -geometry 100 -e 'firefox %s'" % url], shell=True)
       try:
           print "Executar Topologia----> arquivo %s"%variable.topo
           subprocess.call("%s"%variable.mnc,shell=True)
           #print "%s %s %s" % (variable.dir_esp,variable.su,variable.topo)
           output = subprocess.call(["xterm -T 'Topologia-ESP' -geometry 100 -e 'cd static/down_topo && %s python %s' &"%(variable.su,variable.topo)],shell=True)
           os.system("sleep 1")
           return True
           
       except:
           
           print "Falha na execucao da topologia!!"
           return False

def exec_ovs(stovs): # inicializar o ovs e seus servicos
    top = 'top'
    varexovs = stovs
    rm = ['killall ovsdb-server','killall ovs-vswitchd','rmmod openvswitch','rm /usr/local/etc/openvswitch/conf.db','rm /usr/local/etc/openvswitch/.conf.db*']
    
    ovsdb = ['ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema','ovsdb-server -v --remote=punix:/usr/local/var/run/openvswitch/db.sock --remote=db:Open_vSwitch,Open_vSwitch,manager_options --pidfile --detach --log-file=/var/log/openvswitch/ovs-vswitchd.log','ovs-vsctl --no-wait init','ovs-vswitchd --pidfile --detach','modprobe openvswitch']
    
    mk = ['make','make modules_install','make install']

    if varexovs['start'] == "ovs":
       for i in rm:
            print i
            subprocess.call("%s %s"%(variable.su,i),shell=True,stdout=variable.output,stderr=variable.output)

    subprocess.call("cd %s && %s ./boot.sh"%(variable.dir_ovs,variable.su),shell=True,stdout=variable.output,stderr=variable.output)
    print "END ----> boot.sh"
    subprocess.call("sleep 1",shell=True)

    subprocess.call("cd %s && %s ./configure --with-linux=/lib/modules/`uname -r`/build"%(variable.dir_ovs,variable.su),shell=True)
    print "END ---> ./configure"
    subprocess.call("sleep 1",shell=True)

    for k in mk:
        subprocess.call("cd %s && %s %s"%(variable.dir_ovs,variable.su,k),shell=True,stdout=variable.output,stderr=variable.output)
        print "\n-----> comando %s %s"%(variable.su,k)
        #subprocess.call("sleep 1",shell=True)
    os.system("sleep 1")

    try:
    	if varexovs['start'] == "ovs":
       	  for i in ovsdb:
            print i
            subprocess.call("cd %s && %s %s"%(variable.dir_ovs,variable.su,i),shell=True,stdout=variable.output,stderr=variable.output)
            print "camando OVSBB  ----->  %s %s"%(variable.su,i)
            #subprocess.call("sleep 1",shell=True,stdout=variable.output,stderr=variable.output)   
    
    except IndexError, e:
        print "Erro: ", e

    os.system("sleep 1")
    s, r = commands.getstatusoutput("sudo mn --test pingall")
    print r

    if not r:
        return "not"
    else:
        return "ok"

def install_dep(depend): # instalar dependencias(ovs, mininet)
    
    if depend == "install":
        lta = ['build-essential fakeroot debhelper autoconf automake libssl-dev','git-core', 'autoconf', 'automake', 'autotools-dev', 'pkg-config','make gcc g++ libtool', 'libc6-dev cmake libpcap-dev', 'libxerces-c2-dev', 'unzip', 'libpcre3-dev flex bison libboost-dev','gcc make socat psmisc xterm ssh iperf iproute telnet','python-setuptools cgroup-bin ethtool help2man','pyflakes pylint pep8 python-pexpect','autotools-dev pkg-config libc6-dev']
    #lta = ['graphviz', 'python-pygraph']
        for g in lta:
           print "Instalar dependencias....\n"
           print "%s" % g
           subprocess.call("sudo apt-get install -y %s" % g,shell=True,stdout=variable.output,stderr=variable.output)
    print "Dependencias Instaladas"
    os.system("sleep 1")
    return u"yes"

def script_m_ovs(pmn, vovs): # funcao para criar script de instalacao do mininet e OVS 

    #arq = os.popen("find $HOME -name starOVS*").read() #verificar se o arquivo startOVS existe no SO, especificadamente no diretorio home
    #print "%s" % arq
    #mnt = os.popen("mn --version").read()
    #ct = "ok"
    print pmn, vovs

    if vovs == "ovs": # Cria arquivo instalacao do OVS
        print "Criar arquivo de instalacao e inicializacao do OVS...\n"
        t = open("install_OVS.sh", 'wb')
        t.write("#!/bin/sh\n")
        t.write("\n")
	t.write("#mkdir ~/ovs\n")
	t.write("OVS_DIR=$HOME\n")
        t.write("\n")
	t.write("cd $OVS_DIR\n")
	t.write("sudo killall ovsdb-server\n")
	t.write("sudo killall ovs-vswitchd\n")
	t.write("sudo rmmod openvswitch\n")
	t.write("sudo rm /usr/local/etc/openvswitch/conf.db\n") 
	t.write("sudo rm /usr/local/etc/openvswitch/.conf.db*\n")
        t.write("\n")
	t.write("sudo apt-get install -y automake autoconf gcc uml-utilities libtool build-essential pkg-config linux-headers-`uname -r`\n")
	t.write("git clone https://github.com/openvswitch/ovs.git\n")
	t.write("cd ovs\n")
	t.write("./boot.sh\n")
	t.write("./configure --with-linux=/lib/modules/`uname -r`/build\n")
	t.write("make && sudo make modules_install && sudo make install\n")
	t.write("sudo ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema\n")
	t.write("sudo ovsdb-server -v --remote=punix:/usr/local/var/run/openvswitch/db.sock --remote=db:Open_vSwitch,Open_vSwitch,manager_options --pidfile --detach --log-file=/var/log/openvswitch/ovs-vswitchd.log\n")
	t.write("sudo ovs-vsctl --no-wait init\n")
	t.write("sudo ovs-vswitchd --pidfile --detach\n")
	t.write("sudo modprobe openvswitch\n")
        t.write("sudo mn -c\n")

        t.close()
        os.system("chmod 777 " + t.name)
        os.system("chmod +x " + t.name)

    #cmn = raw_input("Criar arquivo de instalacao do mininet, (y=sim, n=nao):")

    if pmn == "mininet":
        print "Criar arquivo de instalacao do mininet...\n"
        mn = open("install_mn.sh",'wb')
        mn.write("#!/bin/bash\n")
        mn.write("\nDIR_HOME=$HOME\n")
	mn.write("\ncd $DIR_HOME\n")
	mn.write("git clone https://github.com/mininet/mininet\n")
	mn.write("cd mininet\n")
	mn.write("util/install.sh -a\n")

        mn.close()
        os.system("chmod 777 " + mn.name)
        os.system("chmod +x " + mn.name)

def install_mininet(imn):
   
    if imn['install'] == "mininet":
        print "Install --->", imn['install']
        s, r = commands.getstatusoutput("ls | grep 'install_mn.sh'")

        if not r:
           print "arquivo de instalacao nao criado"
           return u"not"
        else:
           print "Arquivo de instalacao encontrado"
           subprocess.call('sudo sh %s'%(r),shell=True,stdout=variable.output,stderr=variable.output)
           return u"yes"

def install_ovswitch(instovs):

    if instovs['install'] == "ovs":
        print " -- Instalar OVS.....\n"
        s, r = commands.getstatusoutput("ls | grep 'install_OVS.sh'")

        if not r:
           print "arquivo de instalcao nao criado"
        else:
           print "Arquivo de instalacao encontrado"
           subprocess.call('sudo sh %s'%(r),shell=True,stdout=variable.output,stderr=variable.output)
        
        #print "installl --->", instovs["install"]

def creat_topo(jto,dic_ping,dic_iperf): # cria a topologia de acordo com a lista tp, passada como argumento
    top = jto
    lt = []
    print "del Topo"
    subprocess.call("rm -rf static/down_topo/topo_t.py",shell=True,stdout=variable.output,stderr=variable.output)
    os.system("sleep 1")
    tagcontroll = False
    f = open("topo_t.py", 'wb')
    f.write("#!/usr/bin/python\n")
    f.write("\n")
    f.write("from mininet.net import Mininet\n")
    f.write("from mininet.node import OVSSwitch, Controller, RemoteController, OVSController\n")
    f.write("from mininet.cli import CLI\n")
    f.write("from mininet.log import setLogLevel, info\n")
    f.write("\n") 
    f.write("def ESP_topo():\n")
    f.write("\n")
    for i, j in top['nodes'].iteritems():
        if "switch" in j['group']:
            nets = "switch=OVSSwitch"
            for i, j in top['nodes'].iteritems():
                if "controller" in j['group']:
                    netc = "controller=RemoteController"
                    tagcontroll = True
                    break
                
    if tagcontroll:
        f.write("    net = Mininet( "+ nets + ", " + netc + " )\n")
    else:
        f.write("    net = Mininet( "+ nets + " )\n")

    #f.write("    net = Mininet( controller=Controller)\n" )
    f.write("\n")
    f.write("    info( '*** Adding controller\\n' )\n" )
    print "\n ---- EScrever configuracao do controlador ---- 310\n"
    for i, j in top['nodes'].iteritems():
        print "Tagssss nodes ---- ", j
        if "controller" in j['group']:
            print "Escrever configuracao do Controlador --- 312"
            print "jjjjj ---- ",j
            if "ip" in j:
                print "Escrever IP do controlador --- 314"
                #ipcontroll = "127.0.0.1"
                #controllport = "6633"
                f.write("    " + j["id"] + " = net.addController(name='" + j['label'] + "', ip='" + j['ip'] + "', port=" + j['port'] + " )\n")

    #f.write("    net.addController( 'c0' )\n")
    f.write("\n")
    # adc host e switch a uma lista lt
    #for i in top:
     #  print i
      # lt.append(i[0])
       #lt.append(i[1])
    # remover elementos repetidos da lista lt
    #rr = list(set(lt))
    print "\nADDD NODESSS ---- \n", top['nodes']
    f.write("    info( '*** Adding hosts\\n' )\n")
    nh = 1
    #numip = "10.0.0."
    for i, j in top['nodes'].iteritems():
    	h = "h%s" % nh
    	print "hhhh ---- ", h
        print h, " --- ", i, " --- ", j
        if "desktop" in j["group"]:
           f.write("    "+j['id']+" = net.addHost( '"+j['label']+"' )\n") 
           nh = nh + 1;
    f.write("\n")
    f.write("    info( '*** Adding switch\\n' )\n")
    for i, j in top['nodes'].iteritems():
        print i, " --- ", j['group'], "---", j['label']
        if "switch" in j['group']:
           f.write("    "+j['id']+" = net.addSwitch( '"+j['label']+"' )\n")

    f.write("\n")
    f.write("    info( '*** Add links\\n' )\n")

    taglink = False
    nolink = []

    print "ADDD linkSSS --- ", jto["edges"]

    for i, j in jto['edges'].iteritems():
    	print i, ' --- ', j

    if jto['edges']:   
      for i, l in jto['edges'].iteritems():
        print i, '=>',l
        print l['src'], " -- ",l['dst']

        if ('opts' in l):
           print "has_key Opts encontradooooo"
           opt = l['opts']
           if opt.has_key('link'):
	      taglink = True
              print "Has_key Link encontrada"
              linkopt = l['opts']['link']
           else:
              linkopt = False
              taglink = False
        else:
           taglink = False
           linkopt = False

        print "Opcao linkksss", linkopt

        NameSRC = l['src']
        NameDST = l['dst']
        print "NameSRC -- ", NameSRC['label'], " ,NameDST -- ", NameSRC["label"]     
        optsExist = False
        bw = ''
        delay = ''
        loss = ''
        max_queue_size = ''
        linkOpts = "{"
        print "linha 357"

        if taglink:
            print "Configurar linkkkk"

            if 'bw' in linkopt:
               print "Tag banda encontrada"
	       bw =  linkopt['bw']
               print "LinkOpt bw", bw
               linkOpts = linkOpts + "'bw':"+str(bw)
               optsExist = True

            if 'delay' in linkopt:
	       delay =  linkopt['delay']
               print "LINK delay", delay
               if optsExist:
                  linkOpts = linkOpts + ","
               linkOpts = linkOpts + "'delay':'"+linkopt['delay']+"'"
               optsExist = True

            if 'loss' in linkopt:
               print "OPTS LOSSS", linkopt['loss']
	       if optsExist:
                  linkOpts = linkOpts + ","
               linkOpts = linkOpts + "'loss':"+str(linkopt['loss'])
               optsExist = True
               print "OPTSSS", linkOpts

            if 'max_queue_size' in linkopt:
               if optsExist:
                  linkOpts = linkOpts + ","
               linkOpts = linkOpts + "'max_queue_size':"+str(linkopt['max_queue_size'])
               optsExist = True

            if 'jitter' in linkopt:
                if optsExist:
                   linkOpts = linkOpts + ","
                linkOpts = linkOpts + "'jitter':'"+linkopt['jitter']+"'"
                optsExist = True

            if 'speedup' in linkopt:
                if optsExist:
                   linkOpts = linkOpts + ","
                linkOpts = linkOpts + "'speedup':"+str(linkopts['speedup'])
                optsExist = True

            linkOpts = linkOpts + "}"
            print "LINKoptsss THE END", linkOpts
            if optsExist:
              f.write("    "+NameSRC+NameDST+" = "+linkOpts+"\n")
            f.write("    net.addLink("+NameSRC+", "+NameDST)
            if optsExist:
              f.write(", cls=TCLink , **"+NameSRC+NameDST)
            f.write(" )\n")
        else:
            if (NameSRC['group'] != "controller" and NameDST['group'] != "controller"):
                print "Else add link linha -- 412"
                f.write("    net.addLink("+NameSRC['id']+", "+NameDST['id'])
                f.write(" )\n")
                #f.write("    net.addLink( " +n1+ ", " +n2)
                #f.write(" )\n")
      f.write("\n")
      f.write("    info( '*** Starting switches\\n' )\n")
      for i, l in jto['edges'].iteritems():
        print i, '=>',l
        print l['src'], " -- ",l['dst']

        if ('opts' in l):
           print "has_key Opts encontradooooo"
           opt = l['opts']
           if opt.has_key('link'):
	      taglink = True
              print "Has_key Link encontrada"
              linkopt = l['opts']['link']
           else:
              linkopt = False
              taglink = False
        else:
           taglink = False
           linkopt = False

        print "Opcao linkksss", linkopt

        NameSRC = l['src']
        NameDST = l['dst']
        if (NameSRC['group'] == "controller" or NameDST['group'] == "controller"):
            print "\nAdcionar controlador - Adcionar controladorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"
            print "\nADD controllerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr ---- 453"
            if NameSRC['group'] == "controller":
               getcontroll = NameSRC['id']
               getsw = NameDST['id']
               f.write("    net.get('" + getsw + "').start([" + getcontroll + "])\n")
               print "Controllleee --- NameSRC ----- ", NameSRC
               
            elif NameDST['group'] == "controller":
               getcontroll = NameDST['id']
               getsw = NameSRC['id']
               f.write("    net.get('" + getsw + "').start([" + getcontroll + "])\n")
               print "COntrolllleeee -- NameDST", NameDST

            #f.write("    net.get('" + getsw + "').start([" + getcontroll + "])\n")
             
    f.write("\n")
    f.write("    info( '*** Running test\\n' )\n")

    if dic_ping['ping'] == "Null":
        print "Nao adc trafego ICPM"
    if dic_ping['ping'] != "Null":
       print "Yes, addd traffic ICMP"
       for t, p in dic_ping['ping'].items():
           print t
           print p

    if dic_iperf['iperf'] == "Null":
        print "Not IPERFFFFF"
    if dic_iperf['iperf'] != "Null":
        print "YES IPERFFFF"
        print dic_iperf['iperf']
        if dic_iperf['iperf']['args'] == "default":
            print "Iperf Default"
            print dic_iperf['iperf']['args']
            for t, i in dic_iperf['iperf'].items():
                print t,"------",i
                if t == 'server':
                    #h1.cmdPrint('iperf -s -p 12345 -u &')
                    pser = "%s.cmdPrint('iperf -s &')"%i
                    print pser
                    print "servidor ----- %s=%s"%(t,i)
                    #f.write("    " +pser+"\n")
                    stsv = "1"

                if t == 'client':
                    sv = dic_iperf['iperf']['server']
                    print "servvv --- ", sv
                    pclient = "%s.cmdPrint('iperf -c' + %s.IP())"%(i,sv)
                    print "cliente -----%s=%s"%(t,i)
                    print pclient
                    #if.write("    " +pclient+"\n")
                    stcl = "2"
            if stsv == "1" and stcl == "2":
                print "ok"
                f.write("    " +pser+"\n")
                f.write("    " +pclient+"\n")

        if dic_iperf['iperf']['args'] == "parameter":
            print "Iperf com argumentooooo"
            #h0.cmdPrint('iperf -c ' + h1.IP() +' -u -b 10m -p 12345 -t 10 -i 1')
            for t, i in dic_iperf['iperf'].items():
                print t
                print i

                if t == 'server':
                    #h1.cmdPrint('iperf -s -p 12345 -u &')
                    pser = "%s.cmd('iperf -s -u &')"%i
                    print pser
                    print "servidor ----- %s=%s"%(t,i)
                    #f.write("    " +pser+"\n")
                    stsv = "1"

                elif t == 'client':
                    sv = dic_iperf['iperf']['server']
                    print "servvv --- ", sv
                    pclient = "%s.cmd('iperf -c' + %s.IP() +'"%(i,sv)
                    print "cliente -----%s=%s"%(t,i)
                    print pclient
                    #if.write("    " +pclient+"\n")
                    stcl = "2"
                elif t == "i":
                    tx = i
                    print tx

                elif t == "b":
                    bd = i
                    print "b ----",bd

                elif t == "t":
                    tmp = i
                    print tmp

            iperfargs = "%s -u -i %s -b %s -t %s')"%(pclient,tx,bd,tmp)
            print iperfargs

            if stsv == "1" and stcl == "2":
                print "ok"
                f.write("    " +pser+"\n")
                f.write("    " +iperfargs+"\n")

    f.write("\n")
    f.write("    info( '*** Starting network\\n' )\n")
    f.write("    net.start()\n")
    f.write("\n")
    f.write("    info( '*** Running CLI\\n' )\n")
    f.write("    CLI(net)\n")
    f.write("\n")
    f.write("    info( '*** stopping network' )\n")
    f.write("    net.stop()\n")
    f.write("\n")
    f.write("if __name__ == '__main__':\n")
    f.write("    setLogLevel( 'info' )\n")
    f.write("    ESP_topo()\n")
    f.write("\n")
   
    nn = f.name
    n_topo = variable(nn)
    print "ESP --- Arquivo %s criado com sucesso!!!!"%n_topo.addvar
    print nn   
    f.close()
    
    subprocess.call("sudo chmod +777 topo_t.py",shell=True,stdout=variable.output,stderr=variable.output)
    subprocess.call("mv topo_t.py static/down_topo",shell=True,stdout=variable.output,stderr=variable.output)
    subprocess.call("cd %s rm -rf .*"%variable.dir_esp,shell=True)
    #r_top = script_m_OVS(nn)
    print "Return", top
    return top
        
def ver(nodo): # verifica se o arquivo do switch prototipado foi criado
    print "verificar NO"
    sig = False
    os.system("sleep 1")
    y, x = commands.getstatusoutput('ps -ef | grep mininet: | grep bash | awk \'{print $11}\'')
    m = x.split()

    for i in m:
        if 'mininet' in i:
            print "Nodo Existente"
            sig = True
            break
        else:
            print "Nodod Inexistente!!!"
            continue
            sig = False

    if sig:
    	s = "ok"
        return s

    else:
        s = "not"
  
        return s

@jsonrpc.method('mn')
def mn():

    cm = os.popen("sudo mn").readlines()
  
    return u'Mininet estartado'

@jsonrpc.method('ping')
def ping():

    p = os.popen("ping -10 8.8.8.8").readlines()
    hostname = "google.com" #example
    response = os.system("ping -c 1 " + hostname)

    pg = 'ping'

    if response == 0:
       print hostname, 'is up!'
       pg = 'Rede conectada'
    else:
       print hostname, 'is down!'
       pg = 'down'

    return u'' + pg

@jsonrpc.method('install_dep')
def dependencies(depend):
    dep = t_json(depend)
    tdep = dep['install']
    print tdep
    if tdep == "dependencies":
       depend = "install"
       ridep = install_dep(depend)
       print ridep
       if ridep == "yes":
           print "Dependencias instaladas"
           return u"Dependencias instaladas"
       else:
           print "Dependencias não instaladas"
           return "Dependencias não instaladas"

@jsonrpc.method('match_field')
def regra(field):

     msfield = t_json(field)
     print "Match Field --- ", msfield

@jsonrpc.method('ofctl') #Utilizar a ferramenta ofctl
def dump(of):

     print "offf", of
     msgof = t_json(of)
    
     pr = ['show','dump-ports','dump-flows','del-flows']
     addFlow = ['add-flow']
     nodo = of['nodo']
     ovsof = 'ovs-ofctl'
     su = 'sudo'
     print "Open logiddd --- 648"
     with open('logid.json') as f:
      data = f.read()
      dat =  data.split()
      print " -- data --", data
      print " -- dat -- ", dat
      t = ""
      for i in dat:
        print "--",i
        t = t + i
      print t
      jt = json.loads(json.dumps(t, sort_keys=True, indent = 4))
      print "658 --- ", jt

      s = jt.replace("{" ,"");
      finalstring = s.replace("}" , "");

      lista = finalstring.split(",")

      dit ={}
      print lista
      for i in lista:
        keyvalue = i.split(":")
        m= keyvalue[0].strip('\'')
        m = m.replace("\"", "")
        dit[m] = keyvalue[1].strip('"\'')

      print dit

     nod = False
     idnod = {"swflow":"","ip":"","src":"","dst":""}
     for i, j in dit.iteritems():
         print i," --- ", j
         if i == of['nodo']:
             nod = True
             break;
     if nod:
         print "Nodo encontrado"
     else:
         return "Switch não existe"
     print "Continuar ----- "
     print "APlicar acao no NOde = ", of["nodo"]

     if msgof['command'] == 'ofctl':
        st = ver(nodo) # chama a funcao para verificar a existencia do switch
  
        if st == "ok":
        #if True:
          print "Comando OFFCCTL -> %s" % of['nodo']
          vof = msgof['parameter']
          print "%s --> %s" % (vof,nodo)
          listinfo = []
          print "mensagemm parameterrr", msgof['parameter']
          # manipular as informacoes dispovivel no switch
          for i in pr:
            if msgof['parameter'] == i:
               print "executar %s %s" % (msgof['command'], i)
               paramet = i
               print "%s %s %s %s" % (su, ovsof, paramet, nodo)
               rg = "%s %s %s" % (ovsof, paramet, nodo)
               out = subprocess.Popen([su,ovsof,paramet, nodo], stdout=subprocess.PIPE)
               dumpofctl = out.stdout.readlines()
               for line in dumpofctl: 
	          print line
                  listinfo.append(line)
                  print listinfo
          print  "LISTEEEE ---- INFOOOOOOOOO", listinfo

          # ADD regra, caso a msg json tenha o campo 'add-flow'
          if addFlow[0] == msgof['parameter']:
            print "ADDDD FLOWWWdfsgfasdgasd"
            add = addFlow[0]
            print add
            print msgof['actions']
            print msgof['src']
            r = "regra"
            print "%s %s %s %s %s" % (su, ovsof, add, nodo, r)
            if (msgof['actions'] == "drop") or (msgof['actions'] == "flood"):

               sr = msgof['src']
               ds = msgof['dst']
               actions = msgof['actions']
               verif = False

               for i, j in dit.iteritems():
                   if i == nodo:
                       print i, " --- 731 -- ", j
                       idnod['swflow'] = i
                       
                       for w, y in dit.iteritems():
                           print w, " 735 ----- ", sr
                           if sr == w:
                               verif = True
                               idnod['src'] = y
                               for a, b in dit.iteritems():
                                   print a, " 740--- ", b, " --- ", ds
                                   if ds == a:
                                       print "Switch OpenFlow encontrado, Junto com os siwtchs de origem e destino"
                                       verif = True
                                       idnod['dst'] = b
                                       print "Drop -- for 1 ---"
                                       break;
                                   else:
                                       verif = False

                           if verif:
                                print "Drop for 3 ---"
                                break;

                       if verif:
                            print "Drop --- for 4 --"
                            break;

               if verif:
                             
                    rule = "priority=500, dl_type=0x800, nw_src=%s, nw_dst=%s, actions=%s" % (idnod['src'], idnod['dst'], actions)
                    print rule
                    rg = "%s %s %s" % (add, nodo, rule)
	            #subprocess.Popen([su, ovsof, add, nodo, rule], stdout=subprocess.PIPE)
                    print "comando drop ---- 749"
                    print rg

               else:
                   print "Por favor, verifique nodes informados"
                   return "Por favor, Verifique nodes informados"

            if msgof['actions'] == "FlowInOut":
               InPut = msgof['output']
               OutPut = msgof['input']
               sr = msgof['src']
               ds = msgof['dst']
               rule = "in_port=%s, ip nw_src=%s,ip nw_dst=%s,action=output:%s" % (InPut, idnod['src'], idnod['dst'], OutPut)
               rg =  "%s %s %s %s" % (ovsof, add, nodo, rule) 
               print rg

               verif = False
               for i, j in dit.iteritems():
                   if i == nodo:
                        idnod['swopenflow'] = i 

                        for w, z in dit.iteritems():
                            print sr, " --- ", w
                            if sr == w:
                                verif = True
                                idnod['src'] = z
                        
                                for a, b in dit.iteritems():
                                    if ds == a:
                                        idnod['dst'] = b
                                        verif = True
                                        print "for 1 ---"
                                        break;
                                    else:
                                        verif = False
                            if verif:
                                break;

                   if verif:
                        break;


               if verif:
                    print "Nodes encontrados 787 ---- "
                    rule = "in_port=%s, ip nw_src=%s,ip nw_dst=%s,action=output:%s" % (InPut, idnod['src'], idnod['dst'], OutPut)
                    #subprocess.Popen([su, ovsof, add, nodo, rule], stdout=subprocess.PIPE)
               else:
                    print "Switch Nao Encontrado --- 814"
                    return "Node não encontrado, verifique os Nodes informados"
 
	  return "%s, resultado --> %s" % (rg,listinfo)
           
        else:
          resp = "Switch nao encontrado!"
          return resp
     #return u'%s' % listinfo

@jsonrpc.method('topology_mn') #topologias automatizadas
def topologia(net):

    topomn = t_json(net)
    output = open(os.devnull,'w')
    display = 'xterm -T "CLI - Mininet" -geometry 100 -e'
    sumn = 'sudo mn'
    nw = t_json(net)

    if nw['form'] == 'tree':
         t = nw['form']
         f = nw['fanout']
         d = nw['depth']
         s = nw['switch']
         c = nw['controller']
         i = nw['ip']
         print "Topo -> tree"
         print "%s %s --%s,depth=%s,fanout=%s --switch %s --controle %s"%(display,sumn,t,d,f,s,c)
         for i in topomn:
             print i
         #subprocess.call("",shell=True,stdout=output,stderr=output)

    elif nw['form'] == "linear":
         print "Topo -> Linear"

    elif nw['form'] == 'singler':
         print "Topo -> Singler"

    print nw

@jsonrpc.method('App_topo') # chama a funcao para realizar o parse do msg JSON e em seguida aciona a funcao para prototipara topologia
def network(topo):
	 # print json.dumps(parsed, indent=4, sort_keys=True)
     print topo
     t_json = json.loads(json.dumps(topo))
     print "TESTE NODEEEE", json.dumps(t_json['nodes'], indent=4, sort_keys=True)
     print "\n--------------------------------------------------------------------\n"
     print "Edges --- ", json.dumps(t_json['edges'], indent=4, sort_keys=True)
     jto = {"nodes":{},
     		"links":{},
     		"traffic":{"ping":{},"iperf":{}},
     		"optlinks":{}
     		}
     edges = []
     sn = 1
     hn = 1
     cn = 0
     numip = 1
     idip = open("logid.json", 'wb')
     mapip = "{"
     # Campo nodes(switch/desktop) da msg Json tratado
     for i,j in t_json["nodes"].iteritems():
     	print i, " --- ", j['id'], "--",j['label'], " -- ", j['group']
     	idt, label, group = j['id'], j['label'], j['group'] 
     	links = {
     		"dpid":idt,
     		"id":"",
        	"ip":"",
     		"label":label,
     		"group":group
     	}
     	edges.append(links)
     	#jto["nodes"][label] = links
     	if j['group'] == 'switch':
     	    s = "s%s" % sn
            swip = "10.0.0.%s" % numip
     	    links['id'] = s
            links['ip'] = swip
     	    jto["nodes"][s] = links
     	    sn = sn + 1

     	    if numip == 1:
	        mapip = mapip + "  '"+links['label']+"':'" + links['ip'] + "'";

            if numip > 1:
                mapip = mapip + ","
	        mapip = mapip + "\n   '"+links['label']+"':'"+links['ip'] + "'";

            numip = numip + 1
            print "NumIp switch = " , numip

        if j['group'] == 'controller':
            tags = j
            c = "c%s" % cn
            cport = {"port":""}
            if 'ip' in tags:
                cport['port'] = "6633"
                print "IP encontradooo ---- 902"
                links['id'] = c
                links['ip'] = j['ip']
                links.update(cport)
                jto["nodes"][c] = links
                cn = cn + 1
            else:
                cport['port'] = "6633"
                print "IP default ---- 904"
                links['id'] = c
                links['ip'] = '127.0.0.1'
                print "Porta --- ", cport
                links.update(cport)
                print "942222 --- "
                jto["nodes"][c] = links
                print jto['nodes']
                os.system("sleep 1")
                cn = cn + 1
        print "ADDDDD ---- CONTROLLLERRRRR ----- ", jto
     	print "7644 --- ", sn

     	if j["group"] == 'desktop':
     	    h = "h%s" % hn
            hip = "10.0.0.%s" % numip

     	    links['id'] = h
            links['ip'] = hip
     	    jto["nodes"][h] = links
     	    print "76888 -- ",hn
     	    hn = hn + 1
            if numip == 1:
                print "NumIp = 1  ---- 801"
                mapip = mapip + "  '"+links['label']+"':'"+links['ip']+"'";
                print "NUmIPPP" 
            if numip > 1:
                mapip = mapip + ","
                mapip = mapip + "\n   '"+links['label']+"':'"+links['ip']+"'";
            numip = numip +1
            print "NumIp Host = ", numip
        	
     mapip = mapip + "\n}"
     jmp = json.loads(json.dumps(mapip))
     print "819 --- JMP --- ", jmp
     #subprocess.call("sudo chmod +777 logip.json",shell=True,stdout=variable.output,stderr=variable.output)
     idip.write("" + jmp)
     idip.close()
     subprocess.call("sudo chmod +777 logip.json",shell=True,stdout=variable.output,stderr=variable.output)

     print "EDGESSS ---- ", edges

     #jto["nodes"] = edges

     print "LINKSSS --- 7733333", jto["nodes"]

     for i,j in jto["nodes"].iteritems():
     	print i, " -- ", j

     tp = []
     #jto = {"links":{}}
     num = 1;
     # Campo edges(links) da mensagem Json tratado
     for i,j in t_json["edges"].iteritems():
         src = j['from']
         dst = j['to']
         jto['links'][num] = {"src":src, "dst":dst}
         #tjv['links'][num] = dst 
         print src, " --- ", dst
         num = num + 1; 
         print num
         print "----", jto
     edg = {"edges":{}}
     lk = {"dst":"", "src":""}
     n = 1;
     for w, x in jto['links'].iteritems():
     	src = x['src']
     	dst = x['dst']
     	print "src = ",src, ", dst = ",dst
        lk = {"dst":{"dpid":"", "id":"", "label":"","group":""}, "src":{"dpid":"", "id":"", "label":"", "group":""}}
     	for i, j in jto["nodes"].iteritems():
     		print "80222  -- ", i, " -- ", j
     		print "src = ", src, "dst = ", dst
     		if dst == j['dpid']:
     			 idst = j['id']
     			 ilabel = j['label']
     			 idpid = src

     			 lk['dst']["id"] = idst
     			 lk['dst']['label'] = ilabel
     			 lk['dst']['dpid'] = idpid
                         lk['dst']['group'] = j['group']
     		if src == j['dpid']:
     			 ilabel = j['label']
     			 isrc = j['id']
     			 idpid = src

     			 lk['src']['id'] = isrc
     			 lk['src']['label'] = ilabel
     			 lk['src']['dpid'] = idpid
                         lk['src']['group'] = j['group']
     		print " 822  --- lkkk ",lk

     	print "lkkkk --- ", lk

        edg["edges"][n] = lk
        n = n + 1;
     print "EDGGGG --- ", json.dumps(edg, indent=4, sort_keys=True)

     jto.update(edg)

     print "UPDATEEEE EDGESSS\n", json.dumps(jto["edges"], indent=4, sort_keys=True)

     print "Topology --->> \n", json.dumps(jto, indent=4, sort_keys=True)

     for i, j in jto.iteritems():
         print i, " --- ", j
               
     print "\n"

     if jto.has_key('traffic'):
         print "---------- Tag traffic ----------------"
         vartf = json.loads(json.dumps(jto['traffic']))
         print vartf
         dic_ping = {}
         dic_iperf = {}

         if vartf.has_key('ping'):

            if vartf['ping'] != "Null":
                print "pingggggggggggggggg"
                print vartf['ping']
                dic_ping['ping'] = vartf['ping']
                print "ADDD PINGGGGGGGGGG", dic_ping
            elif vartf['ping'] == "Null":
                print "traffic ping Nullll"
                print vartf['ping']
                dic_ping['ping'] = vartf['ping']
                print "ADDD PINGGG",dic_ping
         else:
            dic_ping = {'ping':'Null'}

         if vartf.has_key('iperf'):

            if vartf['iperf'] != "Null":
                dic_iperf['iperf'] = vartf['iperf']
                print "ADDD IPERFFF", dic_iperf
                print "diction Iperf",dic_iperf
                print dic_iperf['iperf']

            elif vartf['iperf'] == "Null":
                dic_iperf['iperf'] = vartf['iperf']
         #if vartf.has_key('ping'):
          #   ptf = json.loads(json.dumps(vartf['ping']))
           #  print "----------- Trafego PING ----------------"
            # print vartf
             #dic_tf={}
             #dic_tf = vartf
             #print "dic_tf ----",dic_tf
             #print "pin11111----",dic_tf['ping']['1']
             #dic_tf.update({"traffic":"True"})
             #print "update",dic_tf
             #for i, k in dic_tf['ping'].items():
                 #print k
         #h1.cmdPrint('iperf -s -p 12345 -u &')
         #h0.cmdPrint('iperf -c ' + h1.IP() +' -u -b 10m -p 12345 -t 10 -i 1')
         #if vartf.has_key('Null'):
          #   dic_tf = {}
           #  dic_tf['traffic'] = 'Null'
            # print "Nao inserir trafego"

         #if vartf.has_key('iperf'):
          #  itf = json.loads(json.dumps('iperf'))
           # print itf
            #print "---- iperf --------"
         else:
             dic_iperf={"iperf":"Null"}

     else:
         dic_iperf={"iperf":"Null"}
         dic_ping={"ping":"Null"}
         print dic_iperf, dic_ping
    
     if jto.has_key('optlinks'):
        print "Optlins encontrado"
        optl = json.loads(json.dumps(jto['optlinks']))
        print "OPTLINKSSSSS ---- ",optl

     dic_iperf={"iperf":"Null"}
     dic_ping={"ping":"Null"}

     optl = jto["edges"] #passa os links para a função principal

     print "\n Chamar função para prototipar  ------  \n"

     st = creat_topo(jto,dic_ping,dic_iperf) #chama a funcao p/ prototipar a topologia
     print "topo retornada",st
     jto["graph"] = "True"
     print jto
     if jto.has_key('graph'):
         print jto["graph"]

         grapsig = False
     
         if jto["graph"] == "None":
             print "Nao criar topologia em forma de grafos....\n"
    
         if jto['graph'] == "True":
            print "Verificar versao do Ubuntu"
            for i, j in jto["edges"].iteritems():
              print i, "  ----- ", j
              if j.has_key('src'):
                print "srcc encontradooo", j['src']['label']
                sc = j['src']['label']
              if j.has_key('dst'):
                dt = j['dst']['label']
              tp.append([sc,dt])
            
            s, o = commands.getstatusoutput("lsb_release -a")

            out = o.splitlines()

            for i in out:
                if '12' in i:
                    grapsig  = True
                    break

            if grapsig:

                print "Criar topologia em Formato de imagem, versao = 12"
                gt = graph_topo(tp)

            else:

                print "Not create image, version > 12"
     ltop = []
     for z in tp:
         print z
     
     for i in tp:
       ltop.append(i[0])
       ltop.append(i[1])

     numnodes = len(jto["nodes"])
     print "Numero de NODESSSS ----- %s ---- %s" % (jto["nodes"], numnodes)

     numn = len(tp)
     print "nummmm",numn
     rr = list(set(ltop))
     print "rrrrrrrrrr",rr
     nod = len(rr)

     print "tttpppp",tp
     print "NOdos = ",nod
     if numnodes >= 1:
         print "Topologia com numeros de nodos maior que 1"
         return u'Topologia prototipada: Nº de Links = %s, Nº de Nodos = %s' % (numn,numnodes)

     elif numnodes < 1:
         print "Topologia, com numero de nodo menor ou igual a um"

     return u'Nº de Links = %s, Nº de Nodos = %s' % (numn,numnodes)

@jsonrpc.method('app_APS') # Criar arquivo para montar Ambiente de Prototipacao e Simulacao(APS)
def avp(vp):
    jvp = t_json(vp)
    print jvp
    varvp = jvp['aps']
    pvp = json.loads(json.dumps(varvp))
    print varvp
    print pvp['proto']
    if pvp['proto'] == 'mininet':
        pmn = pvp['proto']
        print "criar arquivo de instalacao do Mininet"

        if pvp['virtu'] == 'ovs':
            vovs = pvp['virtu']   
            print "criar arquivo de instalacao do OVS"
            print pmn, "----> ", vovs
            script_m_ovs(pmn, vovs)
            print "Arquivos Criados....\n"
            arqv = "Install_APS"
            #print pvp['virtu']
	else:
 	    print "Arquivos não criados...."

    return "%s" %arqv

@jsonrpc.method('install_mn')
def install_mn(imn):
    instmn = t_json(imn)
    print instmn['install']

    rins = install_mininet(instmn)

    if rins == "not":
        print "Nao foi possivel instalar mininet, arquivo de instalacao nao encontrado"
    
    if rins == "yes":
       rnm = commands.getstatusoutput("sudo mn --test pingall")

       if not rnm:
           print "Erro no mininet"
           return "Erro no mininet"
       else:
           print "Mininet instalado com sucesso"
           return "Mn_instalado com sucesso"

@jsonrpc.method('install_ovs')
def install_ovs(iovs):
    instovs = t_json(iovs)
    #install_ovs(instovs)
    print "OVSSS ---> ", instovs['install']
    install_ovswitch(instovs)
    print "instalar OVS"

    return "ESP - OVS_Instalado" 

@jsonrpc.method('start_topo')
def startTopo(st):
    stp = t_json(st)
    print "Inicializar topologia"
    if stp['start'] == "topology":
        sig = Start_Topo(stp)
        print "sigggggg",sig
        if sig:
            verifNO = ver(sig)
            print "VERIFNOOOOO",verifNO

            if verifNO == "ok":
                 print "Topologia executada com Exito"
                 return "Topologia executada com exito!!"
            elif verifNO == "not":
                 print "Nodos inexistentes, Topologia não inicializada."
                 return "Nodos inexistentes, Topologia não inicializada"
        else:
            return "Nodos inexistentes, Topologia não inicializada"

    #return "OK topologia executada!!"

@jsonrpc.method('kill_proto')
def stoptopo(kill):
    tk = t_json(kill)
    print "Comando stop topologia", tk
    try:
        if tk['command'] == "kill":
            print " ---- Kill Processo Mininet ---- "
            #subprocess.call("ps -ef | grep mininet: | awk \'{print $3}\' | xargs kill", shell=True)
            output = subprocess.call(["xterm -T 'ESP-Stop Topologia' -geometry 100 -e 'sh lib/kill.sh &"],shell=True)
            return "Processos finalizados mininet_stop"
    except:
            return "Erro na finalização mininet_erro"


@jsonrpc.method('start_ovs')
def StartOVS(stovs):
    stovs = t_json(stovs)
    rexe = exec_ovs(stovs)
    print "OpenvSwitch Inicializado",rexe
    if rexe == "ok":
        return u"OpenvSwitch Inicializado"
    if rexe == "not":
        return u"Falha na inicialização do OpenvSwitch"
@jsonrpc.method('limpar_simulacao')
def clean(clean):

    csimul = t_json(clean)

    try: 
        if csimul['simulacao'] == "limpar":
            subprocess.call("sudo mn -c",shell=True,stdout=variable.output,stderr=variable.output)
            #subprocess.call("sudo mn -c",shell=True)
            return r"Limpeza completa"

    except:
        return r"Erro na Limpeza"

@jsonrpc.method('TPost')
def tpost(t):
    print "TESTEEE POST"
    print t
    nm = "TESTTTTT"
    t_json = json.loads(json.dumps(t))
    print t_json['test']
    if t_json['test'] == "marlon":
	print "MARLON"

    os.system("ls -l")
    r = ['1','2','3']
    for i in r:
        print i

    return u'%s' % t_json

@jsonrpc.method('top_vis')
def tpost(t):
    print "Topo testtttt"
    print t
    nm = "TESTTTTT"
    t_json = json.loads(json.dumps(t))
    print "TOPO VIS", t_json
    tjv = {"links":{}}
    num = 1;
    print tjv
    for i,j in t_json.iteritems():
      src = j['from']
      dst = j['to']
      tjv['links'][num] = [src, dst]
      #tjv['links'][num] = dst 
      print src, " --- ", dst
      num = num + 1; 
      print num
      print "----", tjv
    tvis = tjv['links']
    print "TLLLGGGG", tvis
    for i, j in tvis.iteritems():
      print i, " --- ", j

    return u'%s' % t_json

@jsonrpc.method('sair')
def exit(exit):
    print "sair....."
    msgexit = t_json(exit)
    print msgexit
    if 'exit' in msgexit['command']:
        print "Comando Exit em execuçaõ...."
        subprocess.call("ps -ef | grep mininet: | awk \'{print $3}\' | xargs kill & killall firefox", shell=True)
    else:
	print "Not....."
    
    





