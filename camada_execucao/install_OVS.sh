#!/bin/sh

#mkdir ~/ovs
OVS_DIR=$HOME

cd $OVS_DIR
sudo killall ovsdb-server
sudo killall ovs-vswitchd
sudo rmmod openvswitch
sudo rm /usr/local/etc/openvswitch/conf.db
sudo rm /usr/local/etc/openvswitch/.conf.db*

sudo apt-get install -y automake autoconf gcc uml-utilities libtool build-essential pkg-config linux-headers-`uname -r`
git clone https://github.com/openvswitch/ovs.git
cd ovs
./boot.sh
./configure --with-linux=/lib/modules/`uname -r`/build
make && sudo make modules_install && sudo make install
sudo ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema
sudo ovsdb-server -v --remote=punix:/usr/local/var/run/openvswitch/db.sock --remote=db:Open_vSwitch,Open_vSwitch,manager_options --pidfile --detach --log-file=/var/log/openvswitch/ovs-vswitchd.log
sudo ovs-vsctl --no-wait init
sudo ovs-vswitchd --pidfile --detach
sudo modprobe openvswitch
sudo mn -c
