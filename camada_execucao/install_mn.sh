#!/bin/bash

DIR_HOME=$HOME

cd $DIR_HOME
git clone https://github.com/mininet/mininet
cd mininet
util/install.sh -a
