#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import OVSSwitch, Controller, RemoteController, OVSController
from mininet.cli import CLI
from mininet.log import setLogLevel, info

def ESP_topo():

    net = Mininet( switch=OVSSwitch )

    info( '*** Adding controller\n' )

    info( '*** Adding hosts\n' )

    info( '*** Adding switch\n' )
    s1 = net.addSwitch( 's1' )

    info( '*** Add links\n' )

    info( '*** Running test\n' )

    info( '*** Starting network\n' )
    net.start()

    info( '*** Running CLI\n' )
    CLI(net)

    info( '*** stopping network' )
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    ESP_topo()

