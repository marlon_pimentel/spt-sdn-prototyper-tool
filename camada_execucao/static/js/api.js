(function(win) {
    'use strict';

    var $jsonrpc = function(config) {
    	var self = this;
		self.defaults = {
			url: 'http://' + window.location.host + '/api',
			service: undefined,
			version: '2.0'
		};
    	$.extend(true, self.defaults, config || {});

    	self.UUID = function() {
			// http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0,v=c=='x'?r:r&0x3|0x8;return v.toString(16);});
		};

	    self.request = function(name, params) {
	    	if (!!self.defaults.service) {
	    		name = self.defaults.service + '.' + name;
	    	}
			var deferred = $.ajax({
				type: 'POST',
				dataType: 'json',
				url: self.defaults.url,
				data: JSON.stringify({
					'jsonrpc': self.defaults.version,
					'method': name,
					'params': params,
					'id': self.UUID()
				}),
				cache: false
			});

			deferred.done(function(data, textStatus, jqXHR) {
				if (!data.result) {
				   loadclose()
                   		   	   //alert("END --- CLOSEEEEE --- 1111")				 
				   //alert("Resultttt ---- " + JSON.stringify(data.error.stack))
				   var frase = "'" + data.error.stack + "'"
				   var palavra = "creat_topo"
				   //alert("ESP --- Click no icone Editar para criar sua topologia.")
				   if(frase.indexOf(palavra) !== -1){
   				   	// consta sim
   				   	// faça alguma coisa...
    					alert("Click no icone editar para criar topologia.")
  				   	//alert('"'+palavra+'" consta na frase sim!');
				   }

				} else if (data.result) {
				  //alert("Topologia PROTOTIPADAAAAAAAAAAAAAAA --- " + data.result)
				  //alert("END CLOSEEEEE -- 2222")
 				  var texto = "'" + data.result + "'"
				  var grafia = "exito"
                                  	  var inex = "inexistentes"
                                  	  var prototipada = "prototipada"
                                  	  var ovs = "OpenvSwitch"
				  var clear = "Limpeza"
			            var kill = "mininet_stop"
                                  	  var installovs = "OVS_Instalado"
                                  	  var installmn = "Mn_instalado"
				  var aps = "Install_APS"
				  var dep = "Dependencias"
                                          var text = "<ul>";

                                  	   if(texto.indexOf(prototipada) !== -1) {
				   	alert("  Modelagem finalizada, topologia pronta para prototipação!!")
                                        	document.getElementById('topology').style.display = 'block';
					//StopTopo();
				   } else if(texto.indexOf(grafia) !== -1){
    					alert("  Topologia prototipada com êxito!!  ")
                                        	document.getElementById('topology').style.display = 'block';
					StopTopo();
				   } else if (texto.indexOf(inex) !== -1) {
					alert("  Nodos não criados, Topologia não inicializada.  ")
				   } else if (texto.indexOf(ovs) !== -1) {
					alert("  OpenvSwitch Inicalizado  ")
				   } else if (texto.indexOf(clear) !== -1) {
					alert("  Limpeza executada  ")
				   } else if (texto.indexOf(kill) !== -1) {
					alert("  Topologia Finalizada  ")
					//loadclose()
				   } else if (texto.indexOf(installovs) !== -1) {
					alert("  OVS instalado!!  ")
                                                  text += "<li> OVS instalado &#10004</li>";
					text += "</ul>";
           				document.getElementById("Installovs").innerHTML = text;
					//loadclose()
				   } else if (texto.indexOf(installmn) !== -1) {
					alert("  Mininet instalado!!  ")
					text += "<li> Mininet instalado &#10004</li>";
					text += "</ul>";
					document.getElementById("InstallMN").innerHTML = text;
					//loadclose()
				   } else if (texto.indexOf(aps) !== -1) {
					alert("  Arquivos criados!!  ")
					text += "<li> Arquivos criados &#10004</li>";
					text += "</ul>";
           				document.getElementById("ApAPS").innerHTML = text;
					//loadclose()
				   } else if (texto.indexOf(dep) !== -1) {
					alert("  Instalação de dependencias concluida  ")
					text += "<li> Instalação concluida &#10004</li>";
					text += "</ul>";
           				document.getElementById("installdep").innerHTML = text;
					//loadclose()
				   }
				  loadclose()
				  loadclose_monit()
				  //document.getElementById('topology').style.display = 'block';
				}
				
				if ('error' in data) {
					var code = data['error']['code'];
					if (code >= -32768 || code <= -32000) {
						throw data['error']['message'];
					}
				}
			});

			return deferred;
		};
	};

    win.$jsonrpc = $jsonrpc;

})(window);
