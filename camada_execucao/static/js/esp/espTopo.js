function loadJSON(path, success, error) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        success(JSON.parse(xhr.responseText));
      }
      else {
        error(xhr);
      }
    }
  };
  xhr.open('GET', path, true);
  xhr.send();
}

function TopoESP() {
  // criar lista de NÓ
  //alert("TOPO ESP") -- debug
  nodes = new vis.DataSet();
  //nodes.add([
      //{id: 's1', label: 's1', type: ''},
      //{id: 's2', label: 's2',},
      //{id: 's3', label: 's3'},
      //{id: 's4', label: 's4'},
      //{id: 's5', label: 's5'},
      //{id: 's6', label: 's6'}
  //]);

  // Criar lista de Links
  edges = new vis.DataSet();
  //edges.add([
       //{from: 's6', to: 's2', type_from: 'switch', type_to:'host'},
       //{from: 's1', to: 's3'},
       //{from: 's2', to: 's4'},
       //{from: 's2', to: 's5'},
       //{from: 's6', to: 's3'},
       //{from: 's5', to: 's4'}
  //]);
  //alert(JSON.stringify(nodes))

  //return {edges:edges};
  //var topology = new vis.DataSet();
  //alert("tet TOPOOOO" + JSON.stringify(topology))
  //return {edges:topology};
  var topology = {
                nodes: nodes,
                edges: edges
            };
            
  return topology
}

