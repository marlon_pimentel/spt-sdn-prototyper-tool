var ip = "http://10.126.1.245:4080/api"

/* ip - link do servidor */
function showFormElements(oForm) {
   var ip_esp = ip.replace("\"","'")
   //alert(ip)
   var cnt = 0;
   var msg = "Form with 'name' attribute='" + oForm.name + "'";
   var str = "\nThe elements are: \n\n";
   for (i = 0; i < oForm.length; i++) {
	cnt ++; 
	str += oForm.elements[i].tagName + " with 'name' attribute='" + oForm.elements[i].name + "'\n";
   }

   msg += " has " + cnt + " elements. \n" + str;
   //alert(msg);
}

function FormDataESP(oForm) {
   //var ip_esp = 'http://127.0.0.1:4080/api'
   var ip_esp = ip.replace("\"","'")
   //alert(ip)
   var msg = "The data that you entered for the form with 'name' attribute='" + oForm.name + "': \n";
   var jst = {"form":"custom","optlinks":{}}
   var optslinks = {}
   //alert(oForm.elements[0].value)
   var dw = "Download da Topologia"
   var imgTopo = "<img src='static/graph/graph_topo.png' align='center' />"
   var topo = "Download - Topologia em forma de grafo"
   var stop = "<input type='button' for='Prototipar' value='Prototipar' onClick='start_topo()' />"
   var sigtop = "<p id='sigt'>"

   var js = JSON.parse(oForm.elements[0].value)
   jst.links = js
   //alert("R666666666" + oForm.elements[6].value)
   //alert("R77777777777" + oForm.elements[7].value)
   verif = oForm.length
   num = parseInt(verif)
   //alert("" + num)
   var op = Math.trunc(verif / 7);
   //alert("Operação modular" + op)
   var cont = 1;
   // funcao que filtra valores para auxiliar na configuracao dos Links
   //alert("ELEMENTO 11111" + oForm.elements[cont].value)
   if (verif >= 9){
	   //alert("ADDD LINKKKK")
	   for (i=1; i<=op; i++){
		   varl = "link"
           var link = varl.concat(i);
		   var node = {}
		   for(j=1; j<=8; j++){
               
			   if (j==1){
				   var src = oForm.elements[cont].value
				   node.src = src
                   //alert("SRCCCCC" + src)
                   node.name = "test1"
				   cont ++;

			   } else if (j==2){
				   var dst = oForm.elements[cont].value
				   node.dst = dst
                   node.name = "test2"
				   cont ++;

			   } else if (j==3){
                   var delay = oForm.elements[cont].value
                   node.delay = delay
                   cont ++;

               } else if (j==4){
                   var bw = oForm.elements[cont].value
                   node.bw = bw
                   cont ++;

               } else if (j==5){
                   var loss = oForm.elements[cont].value
                   node.loss = loss
                   cont ++;

               } else if (j==6){
                   var maxqueue = oForm.elements[cont].value
                   node.maxqueue = maxqueue
                   cont ++;

               } else if (j==7){
                   var jitter = oForm.elements[cont].value
                   node.jitter = jitter
                   cont ++;

               } else if (j==8){
                   var speedup = oForm.elements[cont].value
                   node.speedup = speedup
                   cont ++;
               }
			   
		   }
		   var n = JSON.stringify(node)
		   var npers = JSON.parse(n)
		   jst.optlinks[i] = npers
	   }
   }
   else{
	   //alert("NOT linkkk")
   }
   
   //alert("Numero de Elementos = " + cont)

   for (i = 0; i < oForm.length, oForm.elements[i].getAttribute("type") !== 'button'; i++) {
	   msg += oForm.elements[i].tagName + " with 'name' attribute='" + oForm.elements[i].name + "' and data: ";
	   //alert(oForm.elements[i].value)
	   
	   if(oForm.elements[i].value == null || oForm.elements[i].value == '') {
		msg += "NOT SET \n";
	   } else {
		   msg += oForm.elements[i].value + "\n";
	   }
   }
   
   //alert(msg);
   //alert(dw)
   var tpost = new $jsonrpc({url: ip_esp, version: '2.0'});
   tpost.request('App_topo',[jst],{name: $('#esp_text').val()}).done(function(data) {
       var dat = data.result;
       //alert('LINKS PARA DOWNLOADSSS' + dat)
       var text = "<ul style='border:0px solid #a1a1a1; margin-before: 1em;'>";
       text += "<li style='list-style-type:none; border: 1px solid black; color: white; font-size:16px; font-family: Courier; background: #444;'> Topologia --> " + dat + "</li>";
       text += "</ul>";
       document.getElementById("ESPTopo").innerHTML = text;
       document.getElementById("download").innerHTML = dw;
       document.getElementById("imgTopo").innerHTML = imgTopo;
       document.getElementById("graph").innerHTML = topo;
       document.getElementById("STOP").innerHTML = stop;
       
   });
   
}

function start_topo() {
   //var ip_esp = 'http://127.0.0.1:4080/api'
   var ip_esp = ip.replace("\"","'")
   //alert("Inicializar Topologia.......") -- debug -- 142
   var jst = {"start":"topology"}
   var stpost = new $jsonrpc({url: ip_esp, version: '2.0'});
   stpost.request('start_topo',[jst],{name: $('#StartTopo').val()}).done(function(data) {
       var dat = data.result;
       // alert('START_TOPOOOOO' + dat) -- debug -- 147
       var text = "<ul>";
       text += "<li style='list-style-type:none; border: 2px solid black; color: white; font-size:14px; padding-left:0.5em; padding-right:0.5em; font-family: Courier; background: #444;'>" + dat + "</li>";
       text += "</ul>";
       document.getElementById("sigt").innerHTML = text;

   });

}

function loadclose_monit() {
    //alert(" --------- Topologia Prototipada ----------- ")
    document.getElementById("loading_monit").style.display="none";
}

function loading_monit() {

    document.getElementById("loading_monit").style.display="block";
}

function loadclose() {
    //alert(" --------- CLosee Loadingg ----------- ")
    document.getElementById("loading").style.display="none";
}

function loading_start() {
    //alert("Chamar Loading ---- ")
    document.getElementById("loading").style.display="block";

}

function loading_proto() {
    var topology = "<li class='tab-link' data-tab='tab-4'>Topologia</li>"
    document.getElementById("loading").style.display="block";
    //document.getElementById("topology").innerHTML = topology;
    //document.getElementById("topology").style.display="block";
    //setTimeout("loadclose()", 5000);
}

function topo() {
    //alert("CHAMAARRR Função PROTOTIPARRRRRRRRRRRRRRRRRRRRR -- OVS MN --- ")
    var ip_esp = ip.replace("\"","'")
    //alert("IPPPPPPPP");
    //alert("DRAWWWW" + JSON.stringify(data.edges['_data'], null, 4))
    // alert("DATTTAAA" + JSON.stringify(topology)) -- debug
    var stop = "<input type='button' for='Prototipar' value='Prototipar' onClick='start_topo();loading_start()' />"
    var startOVS = "<input type='button' for='StartOVS' value='StartOVS' onClick='startOVS();loading_start()' />"
    var clearMN = "<input type='button' for='limpar_mn' value='Limpar' onClick='limpar_simulacao();loading_start()' />"

    var node_list = JSON.stringify(topology.nodes['_data'], null, 4);
    //var node_list_length = JSON.stringify(topology.nodes['length'], null, 4);
    var edge_list = JSON.stringify(topology.edges['_data'], null, 4);
    //var edge_list_length = JSON.stringify(topology.nodes['length'], null, 4);
    
    //alert("TOPO node list -->" + node_list)
    //alert("TOPO edge list -->" + edge_list)

    //concat_list = node_list.concat(edge_list);
    concat_list = '{';
    concat_list = concat_list.concat('"nodes": ');
    concat_list = concat_list.concat(node_list);
    concat_list = concat_list.concat(',');
    concat_list = concat_list.concat('"edges": ');
    concat_list = concat_list.concat(edge_list);
    concat_list = concat_list.concat('}');

    //alert("TOPO concat list -->" + JSON.stringfy(concat_list));
    //alert("TOPO concat list -->" + concat_list); -- Debug
    var dat = "False"
    tjson = JSON.parse(concat_list)
    var stpost = new $jsonrpc({url: ip_esp, version: '2.0'});
    var testdat = stpost.request('App_topo',[tjson]).done(function(data) {
       var dat = data.result;
       testdat.data = data.result;
       //alert('TOPO_VIS' + dat) -- debug -- 195
       var text = "<ul style='border:0px solid #a1a1a1; margin-before: 1em;'>";
       text += "<li style='list-style-type:none; border: 2px solid black; color: white; font-size:14px; padding-left:0.5em; padding-right:0.5em; font-family: Courier; background: #444;'> " + dat + "</li>";
       //text += "</ul>";
       document.getElementById("ESPTopo").innerHTML = text;
       document.getElementById("STOP").innerHTML = stop
       document.getElementById("StartOVS").innerHTML = startOVS;
       document.getElementById("ClearTOPO").innerHTML = clearMN;
       
    });

}



















