#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys
import re
import subprocess
from subprocess import call, PIPE

if __name__ == '__main__':

    ip  = os.popen("ifconfig eth0 | grep \"inet\" | cut -d : -f2 | cut -d \" \" -f2").readlines()
    hostname = "google.com"
    response = os.popen("ping -c 1 " + hostname).readlines()
    output = open(os.devnull,'w')

    if not response:
       print "Conexao Down"
       print "É necessario está conectado com a internet para instalar o Framework ESP"
       print "Pois é preciso instalar os seguintes pacotes:"
       dep = ['python-pip','libgv-python','graphviz','python-pygraph','git','Flask-JSONRPC','Flask','simplejson']
       # Dependencias
       for i in dep:
          print " - %s" % i

    else:
       pg = 'Rede conectada'
       print "%s \n Instalar pacotes....\n" % pg
       apt = ['python-pip','libgv-python','graphviz','python-pygraph','git','python-dev','libevent-dev','python-dev','gcc','xterm','firefox','vim','gedit']
       pip = ['Flask-JSONRPC','Flask','simplejson','--upgrade Six']
       # Dependencias apt
       os.system("sudo apt-get update")

       for i in apt:
          print "Install %s" % i
          os.system("sudo apt-get install -y %s" % i)
       # pip
       for i in pip:
          print "Install %s" % i
          os.system("sudo pip install %s" % i)

       # Checar interface de rede       
       if not ip:
          print "Configurar IP de acesso ao servidor ESP"
          print "Para verificar o IP de sua maquina digite ifconfig -a em outro terminal"
	  infoip = raw_input("Informe o IP: ")
          iprest = os.popen("ping -c 1 " + infoip).readlines()
          port = "4080"

          if iprest:
             url = "http://%s:%s"%(infoip,port)
	     print "IP valido"
             call(["python static/js/esp/write_t.py --ip " + infoip], shell=True)
             call(["xterm -T 'ESP' -geometry 150 -e 'sudo python run_install.py --ip " + infoip + ";' &"], shell=True)
             os.system("sleep 3")
             call(["xterm -T 'ESP' -geometry 100 -e 'firefox %s'" % url], shell=True)
             subprocess.call("rm -rf *~",shell=True)

 
          else:
	     print "IP invalido"          

       elif ip:
          call(["python static/js/esp/write_t.py"], shell=True)
          numip = str(ip[0])
          sp = numip.split()
          Ip_eth0 = sp[0]
          port = "4080"
          url = "http://%s:%s"%(Ip_eth0,port)
          print "Acessar Link --- ESP", url
          call(["xterm -T 'ESP' -geometry 150 -e 'sudo python run_install.py --ip " + Ip_eth0 + ";' &"], shell=True)
          os.system("sleep 3")
          call(["xterm -T 'ESP' -geometry 100 -e 'firefox %s'" % url], shell=True)
          subprocess.call("rm -rf *~",shell=True)
    

    




   
